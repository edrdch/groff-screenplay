This is a groff/mom script to format screenplay
This is a work in progress 

to clone it:
`$ git clone https://framagit.org/edrdch/groff-screenplay.git`

To use it just add this line at the begining of your screenplay.mom:

```
.so /path/to/screen-macro.mom

.SHEAD
INT. PLACE - NIGHT

.ACTION
Describe the action

.CHAR
NAME OF THE CHARACTER
.PAR
some parenthetical
.DIAL
Some dialogues 

.CHAR
ANOTHER CHARACTER
.DIAL
Some words
```




To compile, simple type:

`$ groff -Tpdf -mom screen-test.mom > screen-test.pdf`

